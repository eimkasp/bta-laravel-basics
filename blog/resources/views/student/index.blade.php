@extends('layouts.app')


@section('content')
    <div class="container">

        <table class="table">
            @foreach($students as $student)

                <tr>
                    <td> {{ $student->name }}</td>
                    <td> {{ $student->surname }}</td>
                    <td> {{ $student->email }}</td>
                    <td> {{ $student->phone }}</td>
                    <td>
                        <a href="{{ route('students.show', [$student->id]) }}" class="btn btn-primary">
                            perziureti
                        </a>
                    </td>

                    <td>
                        <form method="post" action="{{ route('students.destroy', [$student->id]) }}">
                            @csrf
                            @method('delete')

                            <input type="submit" class="btn btn-danger" value="X">

                        </form>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>
@endsection