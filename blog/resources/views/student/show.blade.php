@extends('layouts.app')


@section('content')
    <div class="container">

        <div class="row">
            <div  class="col-md-8">
                <h1>{{ $student->name }}</h1>
            </div>

            <div class="col-md-4">
                <div>
                    {{ $student->phone }}
                </div>
                <div>
                    {{ $student->email }}
                </div>
            </div>
        </div>

    </div>
@endsection